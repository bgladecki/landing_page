class ContactController < ApplicationController

	def index
	end

	def sent
		@email = params[:q]
		subscriber = Subscriber.new(email: @email)


		if subscriber.save
			# walidacja?
			redirect_to :contact_thanks
		else
			@error = subscriber.errors.full_messages
			render :index
		end
	end

	def thanks

	end

end
